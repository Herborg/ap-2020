![Screenshot](Skærmbillede 2020-03-09 kl. 00.40.01.png)

[link](https://herborg.gitlab.io/ap-2020/MiniX5)	


For this week’s MiniX i have focused on the miniX2. the reason i have chosen to work on this specific program, is because i feel the feedback i have gotten, has been very relatable. Some of the feedback i got was, that the program made sense and became readable once you had read the readme. Also, i got some feedback from the instructor classes, how i could make the program understandable without the readme, which is one of the reasons for choosing this program.
 
In the original program the emojis were very plain, the only variations were the mouths, and once clicking the mouse, the smiley looked just like only ONE of the previous. But as the idea was that these three people/emojis make the one emoji, this needed to be clearer. Therefore, i added different appearances in the new program, for instance i added aa beard, glasses and hair. All of these three elements are present on the one emoji, that is shown when you click the mouse.
 
This should help the reader understand that these 3 make the 1. due to the fact that these emojis now look a little different from each other, and the fact that single elements are transferred to the big emoji.

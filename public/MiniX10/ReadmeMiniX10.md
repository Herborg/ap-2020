![Screenshot](Flow 1.png)
This idea is that you go through a quiz to see if you have corona. These times everyone is very worried about having corona, and often we overthink and worry, we wanted to create something that visualizes a lot of those thought.

![Screenshot](Flow 2.png)
This idea is also in some related to corona, as well as the environment. Research has shown that since pollution has gone down since the corona virus broke down. With the program we want to visualize that connection.


**Flowchart miniX6:**
![Screenshot](Flowchart.png)
We were asked to create a flowchart of our most technical program. I’m not really sure if this was my most technical program, but at the time of creating it, it was the hardest and the one I found the most technical at the time.

Algorithms are a set of rules, to execute an action or for solving a problem. In the text by
Tania Bucher, she states that algorithms take form in various scales, both software technical, but also the effect on society. In a way flowcharts and algorithms are similar, as they are a set of rules to follow. But the difference as I see it, is the level of complexity in the communication, algorithms need some sort of technical understanding, compared to flowcharts. Flowcharts come in many different forms, they can be technical, detailed or simple, but when it comes to flowchart you get some sort of understanding of how it is connected.

We had a difficult time figuring out just one idea. Then we did a bit of barnstorming on our own, shared with each other and discussed these ideas, as well as added to them. That’s the way we came up with these two ideas. Therefore, we haven’t thought that much about the technical challenges, but, we are aware of meeting challenges. For instance, in idea one, if we’ll be using if-statements, we will have 	a few of them, how do we control these? When it comes to idea number two, we have a technical challenge outside of programming, how can you define air contamination percentage wise, what it 100% contamination, and how do we communicate these? 
They way we would solve this is doing our research beforehand, trying to eliminate any problems beforehand, but of course we can’t expect this to completely solve out problems, but we will have to take them as we go.

The two assignments were quite different, as one was for an existing program, and the other for an idea. When creating a flowchart for an idea, it is helpful to work in a group, because something I might think I understandable might not be the same case for someone else.

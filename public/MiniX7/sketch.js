
var r, g, b;
var w, h;
let x = 0;
let y = 0;

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  background(0)
  //defining the variables
  r = random(255)
  g = random(255)
  b = random(255)
  w = random(20)
  h = random(20)

}
function draw() {
  //this forloop determines the color of the squares, but also creates the effect of the fade in color
  for (var x = 0; x < width; x = x + 25) {
  noFill();
  stroke(x/6, r, b)
//drawing the square
if(random(1) < 0.5){
  rect(x,y,w,h)
//placement of the squares in regards to each other
x+=10;
 if (x > width) {
   x = 0;
 y += 20;
}
}
}
}

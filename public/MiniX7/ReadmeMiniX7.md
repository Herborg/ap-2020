![Screenshot](Skærmbillede 2020-03-22 kl. 14.22.01.png)

https://herborg.gitlab.io/ap-2020/MiniX7

First of all, I have experienced quite a lot of challenges doing this program and the ideas/rules I created beforehand, have been difficult to implement, and some are still lacking. Trying to figure out my issues I did a lot of changes to my code, and I tried to research solutions to my problem, but I was not able to fix it.

The rules I wanted in my generative program were. 1) The squares to change colors in each ‘generation’ and 2) the program to reset once the squares reach the bottom. In the existing program the is a variation in the color of the squares, in the middle of the screen, the colors vary in shade. When reloading the program sizes and colors of the square change. And this is where I wasn’t able to complete and fulfill the set of rules, as you manually have to reload the program. The idea was that it would do it automatically, but I faced some issues with it, and couldn’t find a solution. If I would have been able to complete rule number two, rule number one, would also be a lot clearer, as the color changes are more profound. 


For me the rules were a kind of framework for creating the program. The rules were a baseline and a goal in the process, because they were the reasons the program evolved as it did, and the reason I came to the current idea. In regard to the process, I have faced issues, which resulted in the program not working as intended and as the rules stated in regards to rule number two, because the program does not reset once the squares reach the bottom.

In terms of an auto generator, it is based on some sort of control and autonomy, where a program or the ‘auto generator’ has some sort of control, autonomy and some sort of instructions or rules as in Langtons’s ant colonies, where they behave in different ways, determined beforehand (here the rules being a square to the left or to the right, dependent on its movements). In my program, it has control on positioning the squares as well as the color and sizes. But me as a programmer, I have some control, as I set the rules as well as the frame, that the program executes within.



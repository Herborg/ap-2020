function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(6);

}

function draw() {
 background(70,220);
  drawElements();

  function drawElements() {
  let num = 10; // number of positions the rocket travels in
  push();
  translate(width/2, height/2);//move things to the center

let cir = 360/num*(frameCount);  //to determine where the next rocket sould be placed
rotate(radians(cir));

    //The rocket
  //Red objects on the right and left side of the rocket
  noStroke();
  fill(255,0,0);
  triangle(0,150,30,90,60,150);
  //The body of the rocket
   noStroke();
  fill(0,100,255);
  ellipse(30,90,40,160);
 rect(15,140,30,30)
 //The third red object in the bottom-middle of the rocket
    noStroke();
  fill(255,0,0);
   triangle(22,150,30, 100, 38,150);
  //The window on the rocket
  stroke(255,0,0);
  fill(230);
  ellipse(30,40,20,20);
  //fire at the bottom of the rocket
  fill(255,200,0);
  noStroke();
  rect(16,180,3,30,300)
  rect(28,175,3,30,300)
  rect(40,185,3,30,300)
    pop();

    //planets
    //green planet
    noStroke();
    fill(140,180,0)
  ellipse(90,70,90)
    //blue planet
     noStroke();
    fill(111,186,218)
  ellipse(120,500,130);

  }
}

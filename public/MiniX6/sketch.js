var x, y;
var score = 0;
const radius = 100;
var r, g, b;
var w,h;

function setup() {
  createCanvas(windowWidth, windowHeight);
  x = random(windowWidth);
  y = random(windowHeight);
  r = random(255);
  g = random(255);
  b = random(255);

}

function draw() {
  background(220);

  // All the elements of the doughnuts
  noStroke();
  fill(221, 208, 180);
  ellipse(x, y, r);
  //The icing of the doughnuts
  fill(r, g, b);
  ellipse(x, y, r/1.2);
  // the circle in the middle of the doughnut with the same color as the background
  fill(220)
  ellipse(x,y,r/3)
  //the score
  fill(r,g,b)
  textSize(24);
  text("Score: " + score, 10, 30);
  //"you want doughnuts?" text
  textSize(72);
  text("You want doughnuts?",windowWidth/14,windowHeight/2)
}

// When the user clicks the mouse
function mousePressed() {
  //To check if the mouse is inside of the circle
  var d = dist(mouseX, mouseY, x, y);
  //it is pased on the radius of the circle
  if (d < radius) {
    newCircle();
    score++;
  }
}

//drawing new doughtnut at random places and with random colors
function newCircle() {
  //random places
  x = random(windowWidth);
  y = random(windowHeight);
  //random colors
  r = random(255);
  g = random(255);
  b = random(255);
}

//Speed of intervals
setInterval(newCircle, 1000);

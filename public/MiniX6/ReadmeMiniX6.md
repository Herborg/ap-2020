![Screenshot](MiniX6.png)
[link](https://herborg.gitlab.io/ap-2020/MiniX6)

For this week’s miniX I have created a game with the purpose of catching the doughnuts. When entering the game, you meet a screen with the text "you want doughnuts?" and a single doughnut appearing on the screen as one disappears. The way the game is played is by pressing the doughnuts to increase you score before the doughnut changes position. There is no way of winning the game, but only receiving a high score, also, there is no way of losing the game, you'll just miss a lot of doughnuts.
 
Creating a game, I had great difficulties and had to simplify my code. I had two different sketch files, with the class in one, and the other elements in the other, but I could not get the two .js files to work with each other, that is the reason for the intended class being in the draw section.  The intended class is the doughnuts. The attributes and behaviour can be described as the doughnuts changing in size and changing i colour and the doughnuts change in position.

object-oriented programming is based on the concept of objects, which can contain data or code. And this is where classes usually are used. But as i had a great deal of issues, i have prioritised to create a functional game. Therefore, i haven’t used classes in my code, but the idea was, that the elements of the doughnuts in this case, the elements of the draw function, were supposed to be in the class.
 
The original idea came from "where is Waldo?", which is about locating a person in a certain outfit among a croud of people. Later on, when working on the program a person told me that it reminded him of the 'aim game' in counterstrike, where you get different targets of different sizes on the screen, that you need to shoot. Therefore, one's creations being inspired by one thing, may draw other connections by other people. We may all interpret things in various ways. 

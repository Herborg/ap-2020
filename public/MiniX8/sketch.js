var d = 100;


function preload(){
bg= loadImage('NDF.jpg');
danish = loadJSON("Danish.json");
norwegian = loadJSON("Norwegian.json")
faroese = loadJSON("Faroese.json")
}

function setup() {
  // put setup code here
  createCanvas(900,408);
    background(bg);
}

function draw() {
  //Textsettings
 fill(60, 92, 146);
 textSize(42)
 textAlign(CENTER);
 textFont('Georgia');

 //Heading
 text('Translation', 450, 50)
 //danish words to be translated
 text(danish.one, 450,150) //rar
 text(danish.two, 450,225) //Underlig
 text(danish.three, 450,300) //sjov
 text(danish.four, 450,375) //ked

//rollover for word no.1
push();
 if (dist(mouseX, mouseY, 450, 150) < d/2) {
  text(norwegian.one, 150, 150)
  text(faroese.one, 750, 150)
}
pop();



//rollover for word no.2
push();
if (dist(mouseX, mouseY, 450,225) < d/2) {
 text(norwegian.two, 150, 225)
 text(faroese.two, 750, 225)
}
pop();



//rollover for word no.3
push();
if (dist(mouseX, mouseY, 450,300)< d/2){
 text(norwegian.three, 150, 300)
 text(faroese.three, 750, 300)
}
pop();


//rollover for word no.4
push();
if (dist(mouseX, mouseY, 450,375) < d/2) {
 text(norwegian.four, 150,375)
 text(faroese.four, 750,375)
}
pop();
}

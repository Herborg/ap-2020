![Screenshot](ScreenshotMiniX1.png)
[link](https://herborg.gitlab.io/ap-2020/MiniX1-kopi)

For this first miniX1 i feel like it's overcoming. As to getting to know some of the syntex, the complexity of them vary. Getting to know one syntax, like
the ellipse, I found manageable, but once using several syntaxes together it got more complex if these two should function together. Copying the codes from
the references weren’t demanding in any way. For my own benefits, I tried to create it by myself (rather than copy/paste), as I better would be able to
understand how the different syntaxes work with each offer.

It is very different in the way, that you have to be very precise about what you want to create, in comparison to writing, where you would be able to
understand what was being communicated if there were a few spelling errors. On the other hand, as in programming, there are rules to writing as well, there
are rules on how you construct a sentence, I may say "I love programming" but I can't say "love programming I". Therefore, I can see differences and
similarities.

So far, programming hasn't been such a big part of me. But from the reading materials, and the hands-on practice, it demonstrates how useful it is, and also
helps me understand, that I myself am able to program, and i do look forward to it becoming a bigger part of me.

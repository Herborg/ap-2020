![Screenshot](EmojiSS.png)
[link](https://herborg.gitlab.io/ap-2020/MiniX2)

I can't seem to get my link to work, but you can copy the code from the sketch file.

In this program I have primarily used the ellipse tool, to create the head shape, eyes and them I've used the arc syntax to create the mouths. Furthermore, to create the "effect" of the three emojis becoming one i used the syntax "If(mouseIsPressed)".
 
I come from a little country called the Faroe Islands. We usually say everyone knows everyone, or if that's not the case, at least you know someone that would know that specific person. When you meet you friends’ parents for the first time, they'll ask you "who owns you", referring to who are your parents and grandparents. Who your ancestors are, will assist in the persons opinion about you and their views on you.
 
The threes emojis represent the persons ancestors, all different. Everyone related to the person, will play a part in creating the person’s identity. Your parents and grandparents might have a great reputation in your town, and that reputation will rub off on you. On the other hand, if they don't have a great reputation, that too, will rub off on you, even though you might be nothing like them, they will still shadow your identity.
 
When clicking in the mouse, the single emoji will appear while the other ones disappear. This represents how the identity is formed, the social heritage. No matter how har you try too flea from your ancestors, they will always be a part of you in the way people view you. 
 
When writing the program, i wanted to create an image where several become one to demonstrate the social heritage, and how big a part this plays in a small society.

